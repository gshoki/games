﻿using AutoMapper;
using MS_Games.DTO;
using MS_Games.Models;

namespace MS_Games.Profiles
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            CreateMap<Game, GameDto>();
            CreateMap<GameDto, Game>();
        }
    }
}
