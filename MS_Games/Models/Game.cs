﻿using Components.RestFul;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MS_Games.Models
{
    public class Game : AbstractModel
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("developer")]
        public string Developer { get; set; }

        [Column("publisher")]
        public string Publisher { get; set; }

        [Column("platform")]
        public string Platform { get; set; }
    }
}
