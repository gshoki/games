﻿using Components.RestFul;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MS_Games.Models;

namespace MS_Games.Repositories
{
    public class GameRepository : AbstractRepository<Game>
    {
        public GameRepository(DataContext context, ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .Options;

            if (_context == null)
            {
                _context = new DataContext(options);
            }
        }
    }
}
