﻿using Components.Common.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace MS_Games.Repositories
{
    [ExcludeFromCodeCoverage]
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options) 
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            IEnumerable<Type> listConfiguration = asm.GetTypes().Where(t => t.Name.EndsWith("Configuration"));

            foreach (Type t in listConfiguration)
            {
                dynamic instance = Activator.CreateInstance(t);

                modelBuilder.ApplyConfiguration(Convert.ChangeType(instance, t));
            }

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Environment.GetEnvironmentVariable(FrameworkSettings.ConnectionStringKey));
                optionsBuilder.UseLazyLoadingProxies();
            }
        }
    }
}
