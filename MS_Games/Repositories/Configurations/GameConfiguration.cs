﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MS_Games.Models;

namespace MS_Games.Repositories.Configurations
{
    public class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder.ToTable("GAMES");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Title);
            builder.Property(e => e.Description);
            builder.Property(e => e.Developer);
            builder.Property(e => e.Publisher);
            builder.Property(e => e.Platform);
        }
    }
}
