﻿using Autofac;
using MS_Games.Models;
using MS_Games.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using YPF.MSFramework;
using System.Reflection;
using Autofac.Extensions.DependencyInjection;

namespace MS_Games
{
    public class Startup
    {
        private readonly Bootstrap bt;

        public ILifetimeScope ApplicationContainer { get; private set; }

        public Startup(IConfiguration configuration)
        {
            bt = new Bootstrap();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(b => b.UseLazyLoadingProxies().UseInMemoryDatabase("GamesTmp"));

            // build the Autofac container
            bt.StartUpConfigureServices(services, Configuration, Assembly.GetExecutingAssembly());
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            bt.ConfigureDi(builder, Assembly.GetExecutingAssembly());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            ApplicationContainer = app.ApplicationServices.GetAutofacRoot();

            AddTestData(app);

            app.UseHsts();

            bt.StartUpConfigure(app);
        }

        private void AddTestData(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetService<DataContext>();

            var game = new Game
            {
                Id = 1,
                Title = "The last of us",
                Description = "Es un videojuego de acción-aventura y horror de supervivencia desarrollado por la compañía estadounidense Naughty Dog.",
                Developer = "Naughty Dog",
                Platform = "Playstation",
                Publisher = "Sony"
            };

            var game2 = new Game
            {
                Id = 2,
                Title = "Ghost of Tsushima",
                Description = "Mundo abierto para que los jugadores exploren, el juego gira en torno al último samurái en la isla de Tsushima durante la primera invasión mongola de Japón.",
                Developer = "Sucker Punch",
                Platform = "Playstation",
                Publisher = "Sony"
            };

            context.Add(game);
            context.Add(game2);
            context.SaveChanges();
        }
    }
}
