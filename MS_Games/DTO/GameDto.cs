﻿using Components.Common.DTO;
using Newtonsoft.Json;

namespace MS_Games.DTO
{
    public class GameDto : IRequestDto, IResponseDto
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "developer")]
        public string Developer { get; set; }

        [JsonProperty(PropertyName = "publisher")]
        public string Publisher { get; set; }

        [JsonProperty(PropertyName = "platform")]
        public string Platform { get; set; }
    }
}
