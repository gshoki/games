﻿using AutoMapper;
using Components.Common.DTO;
using Components.Common.Exceptions;
using Components.RestFul;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MS_Games.DTO;
using MS_Games.Models;
using MS_Games.Services;
using System.Collections.Generic;

namespace MS_Games.Controllers
{
    [Route("api/games")]
    [AllowAnonymous]
    public class GameController : AbstractController<Game, GameDto, GameDto>
    {
        private readonly ILogger _logger;

        public GameController(GameService service, ILoggerFactory loggerFactory, IMapper mapper) 
            : base(service, loggerFactory, mapper)
        {
            _logger = loggerFactory.CreateLogger<GameController>();
        }

        public override AbstractResponse<GameDto> PatchComplete(long id, [FromBody] JsonPatchDocument<GameDto> request)
        {
            throw new MethodNotAllowedException(new ErrorResponse(StatusCodes.Status405MethodNotAllowed, "Games", "Cannot patch a Game.", 
                "Api", new Dictionary<string, string>(), null));
        }
    }
}
