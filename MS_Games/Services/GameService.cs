using Components.RestFul;
using Microsoft.Extensions.Logging;
using MS_Games.Models;
using MS_Games.Repositories;

namespace MS_Games.Services
{
    public class GameService : AbstractService<Game>
    {
        private readonly ILogger _logger;

        public GameService(GameRepository repository, ILoggerFactory loggerFactory) : base(repository, loggerFactory)
        {
            this._logger = loggerFactory.CreateLogger<GameService>();
        }
    }
}
